.DEFAULT_GOAL = default
SCRIPT = contrib/scripts/make_scripts.sh

## @ task
.PHONY: env check test docs run
env: ## creates a .env file
	./${SCRIPT} make_env_file

check: ## same as manage.py check
	./${SCRIPT} check

test: ## same as manage.py test
	./${SCRIPT} test

docs: ## same as mkdocs serve, to change port, ex.: make docs 7500
	./${SCRIPT} docs $(filter-out $@,$(MAKECMDGOALS))

run: ## same as manage.py runserver, to change port, ex.: make run 7500
	./${SCRIPT} run $(filter-out $@,$(MAKECMDGOALS))

## @ formatting
.PHONY: black isort format
black: ## run black command
	./${SCRIPT} run_black

isort: ## run isort command
	./${SCRIPT} run_isort

format: isort black ## formatting documents using isort and black

## @ help
.PHONY: help
help: ## display all make commands
	./${SCRIPT} help $(MAKEFILE_LIST)

default: help
