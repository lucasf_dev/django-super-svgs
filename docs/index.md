# Django Super SVGs

Django Super SVGs is a tool for render svgs using Django templatetags.

## Motivation

Do not depend on fonts to display SVG as an icons, as well as have greater freedom to create new ones.

## Instalation

via pip:

```shell
pip install django-super-svgs
```

or poetry:

```shell
poetry add django-super-svgs
```

## Configuration

Add `django_super_svgs` after django apps in `INSTALLED_APPS` on `settings.py`:

```
INSTALLED_APPS = (
    ...
    "django_super_svgs",
)
```
