# Django Super SVGs

## General

- [x] create Django project
- [x] create Dummy app for custom svg creation
- [x] create README.md
- [x] create makefile and shell scripts
- [x] create gitlab ci/cd for run tests and deploy documentation

## Templatetag

- [x] create svgs python files
- [x] create templatetag to render svg
- [x] create css to fix display block to inline-flex

---

## Manage command

- [x] create command to export snippets
- [x] create flag to export to change dir
- [x] create flag to export builtins
- [x] create flag to export to vscode

---

## Tests

### Snippets

- [x] test snippets creation
- [x] test custom folder creation
- [x] test builtins creation
- [x] test creation for vscode

### Templates

- [x] test to verify default css class for container
- [x] test to verify change default css class for container
- [x] test to verify adding some extra classes to container
- [x] test parsing error missing svg filename
- [x] test parsing error missing svg name
- [x] test some changes on attributes
- [x] test creation of svgs inside some app

## Documentation

- [x] How to use
- [x] Customization
- [x] Custom Svgs files
- [x] Snippets
- [x] Builtins
- [x] Contribute
