You can customize the `div container` such as svgs elements.

### Customizing "svg-container" class

If you want to change the default container class name, just add the following variable into `settings.py`

!!! example

    ```python
    SUPER_SVGS_CONTAINER_CLASSES = "my-new-class"
    ```

This way the same SVG entered previously will have the container class `"my-new-class"`

```html
<div class="my-new-class">
  <svg
    width="24"
    height="24"
    fill="none"
    stroke="currentColor"
    stroke-width="1.5"
  >
    <path
      stroke-linecap="round"
      stroke-linejoin="round"
      d="M9 12.75L11.25 15 15 9.75M21 12a9 9 0 11-18 0 9 9 0 0118 0z"
    />
  </svg>
</div>
```

### Adding some extra classes

To add extra class(es) to container just add it as the following example:

```htmldjango
{% svg hero_outline check_circle class="my-new-class text-red" %}
```

it results in:

```html
<div class="svg-container my-new-class text-red">...</div>
```

### Customizing Attributes

Some examples of customization of `svg` and `path` attributes:

!!! example "Examples"

    - svg:fill

    ```htmldjango
    {% svg hero_solid check_circle svg:fill="#ff0000" %}
    ```

    <div class="svg-container">
        <svg width="24" height="24" fill="#ff0000">
            <path fill-rule="evenodd" d="M2.25 12c0-5.385 4.365-9.75 9.75-9.75s9.75 4.365 9.75 9.75-4.365 9.75-9.75 9.75S2.25 17.385 2.25 12zm13.36-1.814a.75.75 0 10-1.22-.872l-3.236 4.53L9.53 12.22a.75.75 0 00-1.06 1.06l2.25 2.25a.75.75 0 001.14-.094l3.75-5.25z" clip-rule="evenodd" />
        </svg>
    </div>

    - svg:stroke

    ```htmldjango
    {% svg hero_solid check_circle svg:stroke="#ff0000" %}
    ```

    <div class="svg-cotainer">
        <svg width="24" height="24" fill="none" stroke="#ff0000" stroke-width="1.5">
            <path stroke-linecap="round" stroke-linejoin="round" d="M9 12.75L11.25 15 15 9.75M21 12a9 9 0 11-18 0 9 9 0 0118 0z" />
        </svg>
    </div>

    - svg:stroke-width

    ```htmldjango
    {% svg hero_solid check_circle svg:stroke="#00569f" svg:stroke-width="3" %}
    ```

    <div class="svg-container">
        <svg width="24" height="24" fill="none" stroke="#ff0000" stroke-width="3">
            <path stroke-linecap="round" stroke-linejoin="round" d="M9 12.75L11.25 15 15 9.75M21 12a9 9 0 11-18 0 9 9 0 0118 0z" />
        </svg>
    </div>

    - path:fill-opacity

    ```htmldjango
    {% svg hero_solid check_circle path:fill-opacity="0.4" %}
    ```

    <div class="svg-container">
        <svg width="24" height="24" fill="currentColor">
            <path fill-rule="evenodd" d="M2.25 12c0-5.385 4.365-9.75 9.75-9.75s9.75 4.365 9.75 9.75-4.365 9.75-9.75 9.75S2.25 17.385 2.25 12zm13.36-1.814a.75.75 0 10-1.22-.872l-3.236 4.53L9.53 12.22a.75.75 0 00-1.06 1.06l2.25 2.25a.75.75 0 001.14-.094l3.75-5.25z" clip-rule="evenodd" fill-opacity="0.4"/>
        </svg>
    </div>

### Customizing in-line

You can add the `css style` attribute into the container `div`:

!!! example "Example"

    ```htmldjango
    {% svg hero_solid check_circle style="transform:scale(2); color:#00c100" %}
    ```

    <div class="md-annotation">
        <div class="svg-container" style="transform:scale(2); color:#00c100">
            <svg width="24" height="24" fill="currentColor">
                <path fill-rule="evenodd" d="M2.25 12c0-5.385 4.365-9.75 9.75-9.75s9.75 4.365 9.75 9.75-4.365 9.75-9.75 9.75S2.25 17.385 2.25 12zm13.36-1.814a.75.75 0 10-1.22-.872l-3.236 4.53L9.53 12.22a.75.75 0 00-1.06 1.06l2.25 2.25a.75.75 0 001.14-.094l3.75-5.25z" clip-rule="evenodd"/>
            </svg>
        </div>
    </div>
