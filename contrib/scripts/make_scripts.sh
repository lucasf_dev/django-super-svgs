#!/usr/bin/env bash

HERE="$(cd "$(dirname "$0")" && pwd)"
BASEDIR="$(cd "$(dirname "$1")" && pwd)"
CHARS="abcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*(-_=+)ABCDEFGHIJKLMNOPQRSTUVWXYZ"
for ((i=0;i<${#CHARS};i++)); do ARRAY[$i]="${CHARS:i:1}"; done

key_gen() {
    for ((c=1; c<=50; c++)); do
        KEY="$KEY${ARRAY[$((RANDOM % ${#CHARS}))]}"
    done
    echo $KEY
}

make_env_file() {
    if [[ ! -f ".env" ]]; then
        ENV="SECRET_KEY='$(key_gen)'\n
        ALLOWED_HOSTS=localhost, 10.0.2.2, 127.0.0.1\n
        DEBUG=True\n\n
        EMAIL_HOST=\n
        EMAIL_PORT=\n
        EMAIL_HOST_USER=\n
        EMAIL_HOST_PASSWORD=\n
        EMAIL_USE_TLS=True\n
        DEFAULT_FROM_EMAIL=
        "

        $(echo -e $ENV | sed -e 's/^[ \t]*//' > .env)
        echo "ENV FILE - $MSG_SUCCESS"
    fi
}

run_black() {
    poetry run black .
}

run_isort() {
    poetry run isort .
}

test() {
    poetry run ./manage.py test django_super_svgs --force-color
}

docs() {
    if [[ -n "$ARG" ]]; then
        poetry run mkdocs serve -a 127.0.0.1:$ARG
    else
        poetry run mkdocs serve
    fi
}

run() {
    if [[ -n "$ARG" ]]; then
        poetry run ./manage.py runserver 127.0.0.1:$ARG
    else
        poetry run ./manage.py runserver
    fi
}

help() {
    awk 'BEGIN {FS="## @ "; print "Usage: make";} /^## @ / { printf "\033[31m\n" substr($1, 5) "\n";} {FS=" ## ";} /^[a-zA-Z_-]+:.*? ##/ { print "\033[33m  -", $1 "\033[37m", $2}' $ARG
}


check() {
    poetry run ./manage.py check
}

ARG=$2
$1
