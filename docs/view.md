Django Super SVGs offers a view to make it easier to choose svgs.

![Image title](assets/view.webp)

The suggestion is to set the url only in debug mode.

```python
...
from django.conf import settings

urlpatterns = [
    path("admin/", admin.site.urls),
]

if settings.DEBUG:
    urlpatterns += [path("svgs/", include("django_super_svgs.urls"))]
```
