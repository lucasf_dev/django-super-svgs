## Creating snippets

The Django Super SVGs provides a `manage.py` command to create `snippets` for your custom svgs.  
To generates your custom snippets, just execute the following command:

```bash
./manage.py create_snippets
```

By default Django Super SVGs create snippets for `neovim` using `vscode` formatting.  
A `snippets` folder will be created at the root of project, containg your snippets files and another one called `package.json`.

```{ .sh .no-copy }
.
├─ snippets/
    └─ mysvg.json
    └─ package.json
```

```{ .json title="mysvg.json" }
{
    "python_logo": {
        "prefix": "svg.mysvg.python_logo",
        "description": "SVG mysvg - python_logo",
        "body": [
            "{% svg mysvg python_logo %}"
        ]
    },
    "tux": {
        "prefix": "svg.mysvg.tux",
        "description": "SVG mysvg - tux",
        "body": [
            "{% svg mysvg tux %}"
        ]
    }
}
```

```{ .json title="package.json" }
{
    "name": "nvim-snippets",
    "author": "Django Super SVGs",
    "engines": {
        "vscode": "^1.11.0"
    },
    "contributes": {
        "snippets": [
            {
                "language": "htmldjango",
                "path": "./mysvg.json"
            }
        ]
    }
}
```

### Changing output directory

To change the output file directory, just use the `-o` flag followed by directory, as code bellow:

```bash
./manage.py create_snippets -o path/to/snippets/
```

### Exporting builtin snippets

To generates builtin svgs provided by Django Super SVGs, just use the `--builtins` flag:

```bash
./manage.py create_snippets --builtins
```

```{ .sh .no-copy }
.
├─ snippets/
    └─ bootstrap.json
    └─ dripicons.json
    └─ hero_outline.json
    └─ hero_solid.json
    └─ mysvg.json
    └─ package.json
```

### Exporting snippets for vscode

In order to export your snippets for `vscode`, just use the `--vscode` flag:

```bash
./manage.py create_snippets --vscode
```

```{ .sh .no-copy }
.
├─ snippets/
    └─ mysvg.code-snippets
```

!!! example "Full example to export to vscode"

    ```bash
    ./manage.py create_snippets --vscode --builtins -o .vscode
    ```

    ```{ .sh .no-copy }
    .
    ├─ .vscode/
        └─ bootstrap.code-snippets
        └─ dripicons.code-snippets
        └─ hero_outline.code-snippets
        └─ hero_solid.code-snippets
        └─ mysvg.code-snippets
    ```
