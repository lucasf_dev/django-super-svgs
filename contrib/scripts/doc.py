from django_super_svgs.templatetags.svgs import (
    bootstrap,
    dripicons,
    hero_outline,
    hero_solid,
    material,
)


def generate(file_name, svg_file):
    svgs = dir(svg_file)
    with open(f"docs/builtins_{file_name}.md", "w") as f:
        for svg in svgs:
            if not svg.startswith("__"):
                s = getattr(svg_file, svg)
                svg_text = f"\\{svg}" if svg.startswith("_") else svg
                text = f"### {svg_text}\n\n"
                text += '<div class="svg-builtins">\n'
                text += f'\t<div class="svg-container">{str(s).replace("\n", "")}</div>\n'
                text += '\t<div class="code">\n'
                text += "\t\t```htmldjango\n"
                text += f"\t\t{{% svg bootstrap {svg} %}}\n"
                text += "\t\t```\n"
                text += "\t</div>\n"
                text += "</div>\n\n"
                f.write(text)

        return text


generate(file_name="bootstrap", svg_file=bootstrap)
generate(file_name="dripicons", svg_file=dripicons)
generate(file_name="hero_outline", svg_file=hero_outline)
generate(file_name="hero_solid", svg_file=hero_solid)
generate(file_name="material", svg_file=material)
