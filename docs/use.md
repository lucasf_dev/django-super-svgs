By default `django-super-svgs` provides these svgs:

- [bootstrap](https://icons.getbootstrap.com/)
- [dripicons](http://demo.amitjakhu.com/dripicons/)
- [heroicons](https://heroicons.com/)
- [material icons](https://github.com/material-icons/material-icons/)

!!! info "heroicons"

    there are two heroicons types:

        . outline
        . solid

Load the `super_svgs` tag on top of your html file, so use the `svg tag` followed by `svg type` [ bootstrap, dripicons, hero_outline, hero_solid, material ] and `svg name`:

```htmldjango
{% load super_svgs %}

{% svg hero_outline check_circle %}
```

### Examples

See below some examples for how to use it:

!!! example "Examples"

    ```htmldjango
    {% svg hero_outline face_smile %}
    ```

    <div class="md-annotation">
        <div class="svg-container">
            <svg width="24" height="24" fill="none" stroke="currentColor" stroke-width="1.5">
                <path stroke-linecap="round" stroke-linejoin="round" d="M15.182 15.182a4.5 4.5 0 01-6.364 0M21 12a9 9 0 11-18 0 9 9 0 0118 0zM9.75 9.75c0 .414-.168.75-.375.75S9 10.164 9 9.75 9.168 9 9.375 9s.375.336.375.75zm-.375 0h.008v.015h-.008V9.75zm5.625 0c0 .414-.168.75-.375.75s-.375-.336-.375-.75.168-.75.375-.75.375.336.375.75zm-.375 0h.008v.015h-.008V9.75z" />
            </svg>
        </div>
    </div>

    ```htmldjango
    {% svg hero_solid face_smile %}
    ```

    <div class="md-annotation">
        <div class="svg-container">
            <svg width="24" height="24" fill="currentColor">
                <path fill-rule="evenodd" d="M12 2.25c-5.385 0-9.75 4.365-9.75 9.75s4.365 9.75 9.75 9.75 9.75-4.365 9.75-9.75S17.385 2.25 12 2.25zm-2.625 6c-.54 0-.828.419-.936.634a1.96 1.96 0 00-.189.866c0 .298.059.605.189.866.108.215.395.634.936.634.54 0 .828-.419.936-.634.13-.26.189-.568.189-.866 0-.298-.059-.605-.189-.866-.108-.215-.395-.634-.936-.634zm4.314.634c.108-.215.395-.634.936-.634.54 0 .828.419.936.634.13.26.189.568.189.866 0 .298-.059.605-.189.866-.108.215-.395.634-.936.634-.54 0-.828-.419-.936-.634a1.96 1.96 0 01-.189-.866c0-.298.059-.605.189-.866zm2.023 6.828a.75.75 0 10-1.06-1.06 3.75 3.75 0 01-5.304 0 .75.75 0 00-1.06 1.06 5.25 5.25 0 007.424 0z" clip-rule="evenodd" />
            </svg>
        </div>
    </div>

    ```htmldjango
    {% svg dripicons alarm %}
    ```

    <div class="md-annotation">
        <div class="svg-container">
            <svg width="24" height="24" fill="currentColor">
                <path d="M21.301 7.975c.281-.255 1.405-1.303 1.431-2.862.026-1.124-.511-2.146-1.584-3.066l-.026-.026c-1.277-.996-2.402-1.047-3.117-.92-1.252.23-2.095 1.125-2.453 1.61A9.847 9.847 0 0 0 12 2.047c-1.227 0-2.427.23-3.526.613-.384-.485-1.201-1.38-2.453-1.61-.716-.127-1.84-.076-3.118.92l-.025.026c-1.099.97-1.636 1.993-1.61 3.117.026 1.559 1.124 2.607 1.43 2.862-.638 1.329-.97 2.811-.97 4.395 0 2.581.97 4.957 2.555 6.772l-.05.05-1.483 2.224a1.022 1.022 0 0 0 .281 1.405.973.973 0 0 0 .562.179.962.962 0 0 0 .843-.46l1.33-1.993A10.203 10.203 0 0 0 12 22.667c2.35 0 4.523-.791 6.26-2.146l1.355 2.02a.99.99 0 0 0 .843.459.92.92 0 0 0 .562-.179c.46-.306.588-.945.281-1.405l-1.482-2.223-.077-.077a10.308 10.308 0 0 0 2.505-6.746 9.708 9.708 0 0 0-.946-4.395zm-2.938-4.88c.434-.077.92.102 1.456.51.588.512.869.997.869 1.483 0 .434-.256.843-.486 1.099-.766-1.023-1.712-1.866-2.785-2.556.256-.255.588-.485.946-.536zM3.313 5.088c0-.486.28-.971.868-1.482.537-.41 1.022-.588 1.456-.511.358.076.69.306.946.536a10.201 10.201 0 0 0-2.785 2.556c-.205-.282-.486-.665-.486-1.1ZM12 20.624c-4.548 0-8.228-3.705-8.228-8.254 0-4.548 3.68-8.279 8.228-8.279s8.228 3.705 8.228 8.254c0 4.548-3.68 8.279-8.228 8.279zm3.705-6.005a1.018 1.018 0 0 1 0 1.456c-.204.205-.46.307-.715.307-.256 0-.511-.102-.716-.307l-2.99-2.99c-.178-.204-.306-.46-.306-.715V6.85c0-.561.46-1.021 1.022-1.021s1.022.46 1.022 1.022v5.085z"/>
            </svg>
        </div>
    </div>

    ```htmldjango
    {% svg bootstrap calendar2_check %}
    ```

    <div class="md-annotation">
        <div class="svg-container">
            <svg width="16" height="16" fill="currentColor">
                <path d="M10.854 8.146a.5.5 0 0 1 0 .708l-3 3a.5.5 0 0 1-.708 0l-1.5-1.5a.5.5 0 0 1 .708-.708L7.5 10.793l2.646-2.647a.5.5 0 0 1 .708 0z"/>
                <path d="M3.5 0a.5.5 0 0 1 .5.5V1h8V.5a.5.5 0 0 1 1 0V1h1a2 2 0 0 1 2 2v11a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V3a2 2 0 0 1 2-2h1V.5a.5.5 0 0 1 .5-.5zM2 2a1 1 0 0 0-1 1v11a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V3a1 1 0 0 0-1-1H2z"/>
                <path d="M2.5 4a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5H3a.5.5 0 0 1-.5-.5V4z"/>
            </svg>
        </div>
    </div>

    ```htmldjango
    {% svg material book_heart %}
    ```

    <div class="md-annotation">
        <div class="svg-container">
            <svg width="24" height="24" fill="currentColor">
                <path d="m19 23.3-.6-.5c-2-1.9-3.4-3.1-3.4-4.6 0-1.2 1-2.2 2.2-2.2.7 0 1.4.3 1.8.8.4-.5 1.1-.8 1.8-.8 1.2 0 2.2.9 2.2 2.2 0 1.5-1.4 2.7-3.4 4.6zM6 22a2 2 0 0 1-2-2V4c0-1.11.89-2 2-2h1v7l2.5-1.5L12 9V2h6a2 2 0 0 1 2 2v9.08L19 13a6.005 6.005 0 0 0-5.2 9z"/>
            </svg>
        </div>
    </div>


The Django Super SVGs encapsulate the `svg tag` inside of a div with class name `svg-container`.

```html
<div class="svg-container">
  <svg
    width="24"
    height="24"
    fill="none"
    stroke="currentColor"
    stroke-width="1.5"
  >
    <path
      stroke-linecap="round"
      stroke-linejoin="round"
      d="M9 12.75L11.25 15 15 9.75M21 12a9 9 0 11-18 0 9 9 0 0118 0z"
    />
  </svg>
</div>
```

!!! warning "Attention"

    The `divs` display their content as `block` by default, which would make each svg display one below the other.


    <div class="md-annotation">
        <div class="">
            <svg width="24" height="24" fill="none" stroke="currentColor" stroke-width="1.5">
                <path stroke-linecap="round" stroke-linejoin="round" d="M15.182 15.182a4.5 4.5 0 01-6.364 0M21 12a9 9 0 11-18 0 9 9 0 0118 0zM9.75 9.75c0 .414-.168.75-.375.75S9 10.164 9 9.75 9.168 9 9.375 9s.375.336.375.75zm-.375 0h.008v.015h-.008V9.75zm5.625 0c0 .414-.168.75-.375.75s-.375-.336-.375-.75.168-.75.375-.75.375.336.375.75zm-.375 0h.008v.015h-.008V9.75z" />
            </svg>
        </div>
        <div class="">
            <svg width="24" height="24" fill="currentColor">
                <path fill-rule="evenodd" d="M12 2.25c-5.385 0-9.75 4.365-9.75 9.75s4.365 9.75 9.75 9.75 9.75-4.365 9.75-9.75S17.385 2.25 12 2.25zm-2.625 6c-.54 0-.828.419-.936.634a1.96 1.96 0 00-.189.866c0 .298.059.605.189.866.108.215.395.634.936.634.54 0 .828-.419.936-.634.13-.26.189-.568.189-.866 0-.298-.059-.605-.189-.866-.108-.215-.395-.634-.936-.634zm4.314.634c.108-.215.395-.634.936-.634.54 0 .828.419.936.634.13.26.189.568.189.866 0 .298-.059.605-.189.866-.108.215-.395.634-.936.634-.54 0-.828-.419-.936-.634a1.96 1.96 0 01-.189-.866c0-.298.059-.605.189-.866zm2.023 6.828a.75.75 0 10-1.06-1.06 3.75 3.75 0 01-5.304 0 .75.75 0 00-1.06 1.06 5.25 5.25 0 007.424 0z" clip-rule="evenodd" />
            </svg>
        </div>
        <div class="">
            <svg width="24" height="24" fill="currentColor">
                <path d="M21.301 7.975c.281-.255 1.405-1.303 1.431-2.862.026-1.124-.511-2.146-1.584-3.066l-.026-.026c-1.277-.996-2.402-1.047-3.117-.92-1.252.23-2.095 1.125-2.453 1.61A9.847 9.847 0 0 0 12 2.047c-1.227 0-2.427.23-3.526.613-.384-.485-1.201-1.38-2.453-1.61-.716-.127-1.84-.076-3.118.92l-.025.026c-1.099.97-1.636 1.993-1.61 3.117.026 1.559 1.124 2.607 1.43 2.862-.638 1.329-.97 2.811-.97 4.395 0 2.581.97 4.957 2.555 6.772l-.05.05-1.483 2.224a1.022 1.022 0 0 0 .281 1.405.973.973 0 0 0 .562.179.962.962 0 0 0 .843-.46l1.33-1.993A10.203 10.203 0 0 0 12 22.667c2.35 0 4.523-.791 6.26-2.146l1.355 2.02a.99.99 0 0 0 .843.459.92.92 0 0 0 .562-.179c.46-.306.588-.945.281-1.405l-1.482-2.223-.077-.077a10.308 10.308 0 0 0 2.505-6.746 9.708 9.708 0 0 0-.946-4.395zm-2.938-4.88c.434-.077.92.102 1.456.51.588.512.869.997.869 1.483 0 .434-.256.843-.486 1.099-.766-1.023-1.712-1.866-2.785-2.556.256-.255.588-.485.946-.536zM3.313 5.088c0-.486.28-.971.868-1.482.537-.41 1.022-.588 1.456-.511.358.076.69.306.946.536a10.201 10.201 0 0 0-2.785 2.556c-.205-.282-.486-.665-.486-1.1ZM12 20.624c-4.548 0-8.228-3.705-8.228-8.254 0-4.548 3.68-8.279 8.228-8.279s8.228 3.705 8.228 8.254c0 4.548-3.68 8.279-8.228 8.279zm3.705-6.005a1.018 1.018 0 0 1 0 1.456c-.204.205-.46.307-.715.307-.256 0-.511-.102-.716-.307l-2.99-2.99c-.178-.204-.306-.46-.306-.715V6.85c0-.561.46-1.021 1.022-1.021s1.022.46 1.022 1.022v5.085z"/>
            </svg>
        </div>
        <div class="">
            <svg width="16" height="16" fill="currentColor">
                <path d="M10.854 8.146a.5.5 0 0 1 0 .708l-3 3a.5.5 0 0 1-.708 0l-1.5-1.5a.5.5 0 0 1 .708-.708L7.5 10.793l2.646-2.647a.5.5 0 0 1 .708 0z"/>
                <path d="M3.5 0a.5.5 0 0 1 .5.5V1h8V.5a.5.5 0 0 1 1 0V1h1a2 2 0 0 1 2 2v11a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V3a2 2 0 0 1 2-2h1V.5a.5.5 0 0 1 .5-.5zM2 2a1 1 0 0 0-1 1v11a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V3a1 1 0 0 0-1-1H2z"/>
                <path d="M2.5 4a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5H3a.5.5 0 0 1-.5-.5V4z"/>
            </svg>
        </div>
        <div class="">
            <svg width="24" height="24" fill="currentColor">
                <path d="m19 23.3-.6-.5c-2-1.9-3.4-3.1-3.4-4.6 0-1.2 1-2.2 2.2-2.2.7 0 1.4.3 1.8.8.4-.5 1.1-.8 1.8-.8 1.2 0 2.2.9 2.2 2.2 0 1.5-1.4 2.7-3.4 4.6zM6 22a2 2 0 0 1-2-2V4c0-1.11.89-2 2-2h1v7l2.5-1.5L12 9V2h6a2 2 0 0 1 2 2v9.08L19 13a6.005 6.005 0 0 0-5.2 9z"/>
            </svg>
        </div>
    </div>

    Django Super SVGs has a `css` file that assigns in the `svg-container` class `inline-flex` display. To use it just add the following into your html `head` section:

    ```html
    <link href="{% static 'django_super_svgs/css/main.css' %}" rel="stylesheet" />
    ```
    <div class="md-annotation">
        <div class="svg-container">
            <svg width="24" height="24" fill="none" stroke="currentColor" stroke-width="1.5">
                <path stroke-linecap="round" stroke-linejoin="round" d="M15.182 15.182a4.5 4.5 0 01-6.364 0M21 12a9 9 0 11-18 0 9 9 0 0118 0zM9.75 9.75c0 .414-.168.75-.375.75S9 10.164 9 9.75 9.168 9 9.375 9s.375.336.375.75zm-.375 0h.008v.015h-.008V9.75zm5.625 0c0 .414-.168.75-.375.75s-.375-.336-.375-.75.168-.75.375-.75.375.336.375.75zm-.375 0h.008v.015h-.008V9.75z" />
            </svg>
        </div>
        <div class="svg-container">
            <svg width="24" height="24" fill="currentColor">
                <path fill-rule="evenodd" d="M12 2.25c-5.385 0-9.75 4.365-9.75 9.75s4.365 9.75 9.75 9.75 9.75-4.365 9.75-9.75S17.385 2.25 12 2.25zm-2.625 6c-.54 0-.828.419-.936.634a1.96 1.96 0 00-.189.866c0 .298.059.605.189.866.108.215.395.634.936.634.54 0 .828-.419.936-.634.13-.26.189-.568.189-.866 0-.298-.059-.605-.189-.866-.108-.215-.395-.634-.936-.634zm4.314.634c.108-.215.395-.634.936-.634.54 0 .828.419.936.634.13.26.189.568.189.866 0 .298-.059.605-.189.866-.108.215-.395.634-.936.634-.54 0-.828-.419-.936-.634a1.96 1.96 0 01-.189-.866c0-.298.059-.605.189-.866zm2.023 6.828a.75.75 0 10-1.06-1.06 3.75 3.75 0 01-5.304 0 .75.75 0 00-1.06 1.06 5.25 5.25 0 007.424 0z" clip-rule="evenodd" />
            </svg>
        </div>
        <div class="svg-container">
            <svg width="24" height="24" fill="currentColor">
                <path d="M21.301 7.975c.281-.255 1.405-1.303 1.431-2.862.026-1.124-.511-2.146-1.584-3.066l-.026-.026c-1.277-.996-2.402-1.047-3.117-.92-1.252.23-2.095 1.125-2.453 1.61A9.847 9.847 0 0 0 12 2.047c-1.227 0-2.427.23-3.526.613-.384-.485-1.201-1.38-2.453-1.61-.716-.127-1.84-.076-3.118.92l-.025.026c-1.099.97-1.636 1.993-1.61 3.117.026 1.559 1.124 2.607 1.43 2.862-.638 1.329-.97 2.811-.97 4.395 0 2.581.97 4.957 2.555 6.772l-.05.05-1.483 2.224a1.022 1.022 0 0 0 .281 1.405.973.973 0 0 0 .562.179.962.962 0 0 0 .843-.46l1.33-1.993A10.203 10.203 0 0 0 12 22.667c2.35 0 4.523-.791 6.26-2.146l1.355 2.02a.99.99 0 0 0 .843.459.92.92 0 0 0 .562-.179c.46-.306.588-.945.281-1.405l-1.482-2.223-.077-.077a10.308 10.308 0 0 0 2.505-6.746 9.708 9.708 0 0 0-.946-4.395zm-2.938-4.88c.434-.077.92.102 1.456.51.588.512.869.997.869 1.483 0 .434-.256.843-.486 1.099-.766-1.023-1.712-1.866-2.785-2.556.256-.255.588-.485.946-.536zM3.313 5.088c0-.486.28-.971.868-1.482.537-.41 1.022-.588 1.456-.511.358.076.69.306.946.536a10.201 10.201 0 0 0-2.785 2.556c-.205-.282-.486-.665-.486-1.1ZM12 20.624c-4.548 0-8.228-3.705-8.228-8.254 0-4.548 3.68-8.279 8.228-8.279s8.228 3.705 8.228 8.254c0 4.548-3.68 8.279-8.228 8.279zm3.705-6.005a1.018 1.018 0 0 1 0 1.456c-.204.205-.46.307-.715.307-.256 0-.511-.102-.716-.307l-2.99-2.99c-.178-.204-.306-.46-.306-.715V6.85c0-.561.46-1.021 1.022-1.021s1.022.46 1.022 1.022v5.085z"/>
            </svg>
        </div>
        <div class="svg-container">
            <svg width="16" height="16" fill="currentColor">
                <path d="M10.854 8.146a.5.5 0 0 1 0 .708l-3 3a.5.5 0 0 1-.708 0l-1.5-1.5a.5.5 0 0 1 .708-.708L7.5 10.793l2.646-2.647a.5.5 0 0 1 .708 0z"/>
                <path d="M3.5 0a.5.5 0 0 1 .5.5V1h8V.5a.5.5 0 0 1 1 0V1h1a2 2 0 0 1 2 2v11a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V3a2 2 0 0 1 2-2h1V.5a.5.5 0 0 1 .5-.5zM2 2a1 1 0 0 0-1 1v11a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V3a1 1 0 0 0-1-1H2z"/>
                <path d="M2.5 4a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5H3a.5.5 0 0 1-.5-.5V4z"/>
            </svg>
        </div>
        <div class="svg-container">
            <svg width="24" height="24" fill="currentColor">
                <path d="m19 23.3-.6-.5c-2-1.9-3.4-3.1-3.4-4.6 0-1.2 1-2.2 2.2-2.2.7 0 1.4.3 1.8.8.4-.5 1.1-.8 1.8-.8 1.2 0 2.2.9 2.2 2.2 0 1.5-1.4 2.7-3.4 4.6zM6 22a2 2 0 0 1-2-2V4c0-1.11.89-2 2-2h1v7l2.5-1.5L12 9V2h6a2 2 0 0 1 2 2v9.08L19 13a6.005 6.005 0 0 0-5.2 9z"/>
            </svg>
        </div>
    </div>

    Or just use your desired class.
