:heart: :smiley: Thanks for being interested in contributing to the Django Super SVGs project.  
In this document are listed the most common operations that you may need to contribute.

# How to

Here are listed useful commands in order to help you to contribute.

## Tools

!!! info

    This project uses [Poetry](https://python-poetry.org){:target="_blank"} as dependency management and packaging. So be sure to install it first.

### Cloning the project

```bash
git clone https://gitlab.com/lucasf_dev/django-super-svgs.git
```

### Installing dependencies

```bash
poetry install
```

### Displaying make help command

```bash
make
```

or

```bash
make help
```

### Create .env file

```bash
make env
```

### Formatting

```bash
make format
```

### Run tests

```bash
make test
```

### Serve documentation

```bash
make docs
```

## I didn't find what i need here

If you have not found what you need, you can open an issue in the project.  
You can report what you can't do or what it needs to be best documented.

## Continuous improvement

This document can be improved by anyone who is interested in improving it.  
So feel free to provide more tips to people who want to contribute too. :smiley:
